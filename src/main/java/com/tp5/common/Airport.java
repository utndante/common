package com.tp5.common;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "airports")
public class Airport {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false, name = "name", length = 50)
    private String name;

    @Column(nullable = false, name = "iata_code", length = 80)
    private String iata;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

}
