package com.tp5.common;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "cabins")
public class Cabin {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "name", length = 50)
    private String name;

    // Esto se podria hacer pero NO hace falta.. ya que no nos interesa listar las cabinas con sus vuelos
    /*@OneToMany(mappedBy = "cabin", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("cabin")
    private List<Flight> flights;*/

}
